# DE
```julia
include("DE.jl")
using Plots; pyplot()
```
```julia
f(x) = x[1]^2 + sin(10*x[1]) + 2*cos(7*x[1])
plot(f, size=(700,300), xlims=(-4,4), linewidth=1.7)
```
![](fig/pl1.png "plot of f(x)")
```julia
DifferentialEvolution(f,[(-4,4)])
(x_min = [0.4555], f_min = -2.777959754970113, Nit = 37)
```

```julia
rosen(x) = x[1]^2 + 100*(x[2]-x[1]^2)^2
x=-2:0.1:2
y=-2:0.1:4
plot(x,y,(x,y)->rosen([x,y]),st=:surface,camera=(45,45),colorbar_entry=false)
```
![](fig/3d.png "plot of rosen")
```julia
DifferentialEvolution(rosen,[(-2,2),(-2,2)]) 
(x_min = [-2.20104e-6, -5.01168e-7], f_min = 2.996205008477173e-11, Nit = 74)
```
