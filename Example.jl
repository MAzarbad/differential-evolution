# ---
# jupyter:
#   jupytext:
#     formats: ipynb,jl:light
#     text_representation:
#       extension: .jl
#       format_name: light
#       format_version: '1.4'
#       jupytext_version: 1.2.1
#   kernelspec:
#     display_name: Julia 1.1.1
#     language: julia
#     name: julia-1.1
# ---

include("DE.jl")
using Plots; pyplot()

f(x) = x[1]^2 + sin(10*x[1]) + 2*cos(7*x[1])

plot(f, size=(700,300), xlims=(-4,4), linewidth=1.5)
savefig("pl1.png")

DifferentialEvolution(f,[(-4,4)])

rosen(x) = x[1]^2 + 100*(x[2]-x[1]^2)^2
x=-2:0.1:2
y=-2:0.1:4
plot(x,y,(x,y)->rosen([x,y]),st=:surface,camera=(45,45),colorbar_entry=false)
savefig("3d.png")

DifferentialEvolution(rosen,[(-2,2),(-2,2)]) 


