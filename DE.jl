function DifferentialEvolution(fun::Function, bounds; eps  = 1e-5,
				feps = 1e-10, mut=0.6, crossp=0.7, popsize=15, its=5000)
	eps2=eps^2
    dim = length(bounds)
    pop = rand(popsize, dim); pop_denorm = zeros(popsize, dim)
    min_b = zeros(dim);	max_b = zeros(dim); diff = zeros(dim)
    for j in 1:dim
        min_b[j] = bounds[j][1];	max_b[j] = bounds[j][2]
        diff[j]  = max_b[j] - min_b[j]
        for k in 1:popsize	pop_denorm[k,j] = min_b[j] + pop[k,j]*diff[j]	end
    end

    idxs = Array(1:popsize);
    mutant = zeros(dim)
    cross_points = trues(dim)
    trial = zeros(dim)
    trial_denorm = zeros(dim)
    best = zeros(dim);	bestTemp = zeros(dim)
    fitness = zeros(popsize)

    for i in 1:popsize	fitness[i] = fun(pop_denorm[i,:]) end
	best_idx = argmin(fitness)
	for k in 1:dim	best[k] = pop_denorm[best_idx,k] end

	clip(x::Float64) = (x > 1) + (0 <= x <= 1) * x
	function Sample3(x::Array{Int,1})
		n = length(x);
		i1 = rand(1:n);		y1 = x[i1];	deleteat!(x, i1)
		i2 = rand(1:(n-1)); y2 = x[i2];	deleteat!(x, i2)
		y3 = x[rand(1:(n-2))]
		return [y1, y2, y3]
	end

    for i in 1:its
	bestfitTemp = fitness[best_idx];	bestTemp .= best
        for j in 1:popsize
            abc = pop[Sample3(idxs[idxs .!= j]),:]
            for k in 1:dim
                mutant[k] = clip(abc[1,k] + mut * (abc[2,k] - abc[3,k]))
                cross_points[k] = rand() < crossp
            end
            if !any(cross_points)	cross_points[rand(1:dim)] = true  end
            for k in 1:dim
                trial[k] = cross_points[k]*mutant[k]+(1-cross_points[k])*pop[j,k]
                trial_denorm[k] = min_b[k] + trial[k]*diff[k]
            end
            f = fun(trial_denorm)
            if f < fitness[j]
                fitness[j] = f
                for k in 1:dim	pop[j,k] = trial[k]	end
                if f < fitness[best_idx]
                    best_idx = j
                    for k in 1:dim	best[k] = trial_denorm[k]	end
                end
            end
        end
        if ( (0 < abs(fitness[best_idx] - bestfitTemp) < feps) &
			(sum((bestTemp-best).^2) < eps2) )
           return (x_min=best, f_min=fitness[best_idx], Nit = i)
        end
    end
	#println("maximum number of iterations reached.")
    return (x_min=best, f_min=fitness[best_idx], Nit = its)
end

